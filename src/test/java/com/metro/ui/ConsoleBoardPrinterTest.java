package com.metro.ui;

import static org.junit.Assert.*;

import org.junit.Test;

public class ConsoleBoardPrinterTest {

	@Test
	public void shouldPrintBoard() {
		ConsoleBoardPrinter printer = new ConsoleBoardPrinter();
		String[][] board = {{"a", "b"},	{"c", "d"}};
		
		String statusPrint = printer.printBoard(board);
		
		assertTrue(statusPrint.contains("a"));
		assertTrue(statusPrint.contains("b"));
		assertTrue(statusPrint.contains("c"));
		assertTrue(statusPrint.contains("d"));
	}

}
