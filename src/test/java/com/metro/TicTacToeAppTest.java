package com.metro;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyListOf;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.metro.config.GameConfig;
import com.metro.game.GameEngine;
import com.metro.model.player.Player;
import com.metro.model.player.builder.PlayerBuilder;
import com.metro.ui.io.Writer;
import com.metro.validate.ArgumentsValidator;

@RunWith(SpringRunner.class)
public class TicTacToeAppTest {
	
	@MockBean
	GameConfig config;
	
	@MockBean
	ArgumentsValidator validator;	
	
	@MockBean
	GameEngine gameEngine;
	
	@MockBean
	ShutdownManager shutdownManager;	
	
	@MockBean
	PlayerBuilder playerBuilder;
	
	@MockBean
	Writer out;
	
	@InjectMocks
	TicTacToeApp app;
	
	@Before
	public void prepare() {
	    MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldValidateInputArguments() throws Exception {
		
		String[] characters = {"x", "o"};
		doNothing().when(validator).validatePlayerCharacters();
		doNothing().when(validator).validatePlaygroundSize();
		
		when(config.getPlayerCharacters()).thenReturn(characters);
		
		doNothing().when(gameEngine).start();
		
		app.run("");
		
		verify(validator, times(1)).validatePlayerCharacters();
		verify(validator, times(1)).validatePlayerCharacters();		
	}
	
	@Test
	public void shouldErrorWhenPlayerCharacterValidationFails() throws Exception {
		String[] characters = {"x", "o"};
		when(config.getPlayerCharacters()).thenReturn(characters);
		
		doThrow(new IllegalArgumentException("invalid")).when(validator).validatePlayerCharacters();
		
		app.run("");

		verify(out, times(1)).write("invalid");
		verify(shutdownManager, times(1)).initiateShutdown(-1);
	}
	
	@Test
	public void shouldErrorWhenPlaygroundValidationFails() throws Exception {
		String[] characters = {"x", "o"};
		when(config.getPlayerCharacters()).thenReturn(characters);
		
		doNothing().when(validator).validatePlayerCharacters();		
		doThrow(new IllegalArgumentException("invalid")).when(validator).validatePlaygroundSize();
		
		app.run("");

		verify(out, times(1)).write("invalid");
		verify(shutdownManager, times(1)).initiateShutdown(-1);
	}
	
	@Test
	public void shouldAddAllPlayersToGame() throws Exception {
		String[] characters = {"x", "o"};
		
		Player player1 = mock(Player.class);
		Player player2 = mock(Player.class);
		
		doNothing().when(validator).validatePlayerCharacters();
		doNothing().when(validator).validatePlaygroundSize();
		
		when(config.getPlayerCharacters()).thenReturn(characters);
		when(playerBuilder.buildPayersFromCharacters(anyListOf(String.class))).thenReturn(Arrays.asList(player1, player2));
		
		doNothing().when(gameEngine).addPlayer(any(Player.class));
		doNothing().when(gameEngine).start();
		
		app.run("");
		
		verify(gameEngine, times(2)).addPlayer(any(Player.class));
	}
	
	@Test
	public void shouldStarTheGame() throws Exception {
		String[] characters = {"x", "o"};
		
		Player player1 = mock(Player.class);
		Player player2 = mock(Player.class);
		
		doNothing().when(validator).validatePlayerCharacters();
		doNothing().when(validator).validatePlaygroundSize();
		
		when(config.getPlayerCharacters()).thenReturn(characters);
		when(playerBuilder.buildPayersFromCharacters(anyListOf(String.class))).thenReturn(Arrays.asList(player1, player2));
		
		doNothing().when(gameEngine).addPlayer(any(Player.class));
		doNothing().when(gameEngine).start();
		
		app.run("");
		
		verify(gameEngine, times(1)).start();
	}



}
