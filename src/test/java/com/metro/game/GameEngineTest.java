package com.metro.game;

import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.metro.config.GameConfig;
import com.metro.game.exception.InvalidMoveException;
import com.metro.model.Board;
import com.metro.model.player.Player;
import com.metro.model.Position;
import com.metro.ui.BoardPrinter;
import com.metro.ui.io.Writer;

@RunWith(SpringRunner.class)
public class GameEngineTest {
	
	@MockBean
	GameConfig config;
	
	@MockBean
	Board board;
	
	@MockBean
	BoardPrinter boardPrinter;
	
	@MockBean
	Greeter greeter;
	
	@MockBean
	Writer out;
	
	@InjectMocks
	GameEngine engine;

	@Before
	public void prepare() {
	    MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldBeAbleAddPlayers() {
		Player player = mock(Player.class);
		engine.addPlayer(player);
		
		assertTrue(engine.getPlayers().size() == 1);
	}
	
	@Test
	public void getsMovesFromPlayers() {
		Position expectedPosition = Position.buildFromInput("1,2");
		Player player = mock(Player.class);
		when(player.getNextMove()).thenReturn(expectedPosition);
		
		Position actualPosition = engine.getNextMoveFrom(player);
		
		assertTrue(expectedPosition.equals(actualPosition));		
	}
	
	@Test
	public void promptsPlayerAgainWhenNextMoveThrowsInvalidMoveException() {
		Position validPosition = Position.buildFromInput("1,2");
		
		Player player = mock(Player.class);
		when(player.getNextMove())
						.thenThrow(new InvalidMoveException("invalid"))
						.thenThrow(new InvalidMoveException("invalid"))
						.thenReturn(validPosition);
		
		Position actualPosition = engine.getNextMoveFrom(player);	
		
		assertTrue(actualPosition.equals(validPosition));
		verify(out, times(2)).write("invalid");
	}
	
	@Test
	public void promptsPlayerAgainWhenNextMoveThrowsNumberException() {
		Position validPosition = Position.buildFromInput("1,2");
		
		Player player = mock(Player.class);
		when(player.getNextMove())
						.thenThrow(new NumberFormatException())
						.thenThrow(new NumberFormatException())
						.thenReturn(validPosition);
		
		Position actualPosition = engine.getNextMoveFrom(player);	
		
		assertTrue(actualPosition.equals(validPosition));
		verify(out, times(2)).write(anyString());
	}

	@Test
	public void promptsPlayerAgainWhenBoardThorwsIllegalArgumentException() {
		Position validPosition = Position.buildFromInput("1,2");
		Position invalidPosition = mock(Position.class);
		
		Player player = mock(Player.class);
		when(player.getNextMove())
						.thenReturn(invalidPosition)
						.thenReturn(validPosition);
		
		doThrow(new IllegalArgumentException()).when(board).move(player, invalidPosition);
		doNothing().when(board).move(player, validPosition);
		
		engine.makeMove(player);	
		
		verify(out, times(1)).write(anyString());
		verify(board, times(2)).move(any(Player.class), any(Position.class));
		verify(player, times(2)).getNextMove();
	}
	
	@Test
	public void testGameFlow() {
		String[][] boardStatus = {{"0,0"}};
		
		Position position = mock(Position.class);
		
		Player player1 = mock(Player.class);
		Player player2 = mock(Player.class);
		Player player3 = mock(Player.class);
		
		when(player1.getNextMove()).thenReturn(position);
		when(player2.getNextMove()).thenReturn(position);
		when(player3.getNextMove()).thenReturn(position);
		
		doNothing().when(board).move(any(Player.class), any(Position.class));
		doNothing().when(board).checkForWin();
		when(board.getCurrentStatus()).thenReturn(boardStatus);
		
		when(boardPrinter.printBoard(boardStatus)).thenReturn("0,0");
		doNothing().when(out).write(anyString());		
		
		when(board.isBoardFull())
						.thenReturn(false)
						.thenReturn(false)
						.thenReturn(false)
						.thenReturn(true);
		
		engine.addPlayer(player1);
		engine.addPlayer(player2);
		engine.addPlayer(player3);
		
		engine.start();
		
		verify(out, times(1)).write("Match draw. Well played everyone!");
		verify(player1, times(1)).getNextMove();
		verify(player2, times(1)).getNextMove();
		verify(player3, times(1)).getNextMove();
		verify(board, times(4)).isBoardFull();
		verify(board, times(3)).move(any(Player.class), any(Position.class));
	}
	

}
