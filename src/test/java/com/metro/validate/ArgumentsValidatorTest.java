package com.metro.validate;

import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.metro.config.GameConfig;

@RunWith(SpringRunner.class)
public class ArgumentsValidatorTest {
	
	@MockBean
    GameConfig config;
	
	@InjectMocks
	ArgumentsValidator validator;
	
	@Before
	public void prepare() {
	    MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void shouldValidatePlaygroundSizeForValidInputs() {
		when(config.getMaxPalygroundSize()).thenReturn(10);
		when(config.getMinimumPalygroundSize()).thenReturn(3);
		when(config.getPlaygroundSize()).thenReturn(6);
		
		validator.validatePlaygroundSize();		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowExceptionWhenGivenSizeIsLessThanMinimum() {
		when(config.getMaxPalygroundSize()).thenReturn(10);
		when(config.getMinimumPalygroundSize()).thenReturn(3);
		when(config.getPlaygroundSize()).thenReturn(2);
		
		validator.validatePlaygroundSize();		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowExceptionWhenGivenSizeIsMoreThanMaximum() {
		when(config.getMaxPalygroundSize()).thenReturn(10);
		when(config.getMinimumPalygroundSize()).thenReturn(3);
		when(config.getPlaygroundSize()).thenReturn(11);
		
		validator.validatePlaygroundSize();		
	}

	@Test
	public void shouldValidatePlayerCharactersForValidInput() {
		String [] characters = {"a","b","c"};
		when(config.getTotalPlayers()).thenReturn(3);
		when(config.getPlayerCharacters()).thenReturn(characters);
		
		validator.validatePlayerCharacters();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThowExceptionWhenCharactersAreLessThanNumberOfPlayers() {
		String [] characters = {"a","b"};
		when(config.getTotalPlayers()).thenReturn(3);
		when(config.getPlayerCharacters()).thenReturn(characters);
		
		validator.validatePlayerCharacters();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThowExceptionWhenAnyCharacterIsGreaterThanOneLength() {
		String [] characters = {"a","bc", "c"};
		when(config.getTotalPlayers()).thenReturn(3);
		when(config.getPlayerCharacters()).thenReturn(characters);
		
		validator.validatePlayerCharacters();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThowExceptionWhenAnyReservedCharactersIsUsed() {
		String [] characters = {"a","-", "d"};
		when(config.getTotalPlayers()).thenReturn(3);
		when(config.getPlayerCharacters()).thenReturn(characters);
		
		validator.validatePlayerCharacters();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThowExceptionWhenAnyDuplicateCharactersAreUsed() {
		String [] characters = {"a","a", "d"};
		when(config.getTotalPlayers()).thenReturn(3);
		when(config.getPlayerCharacters()).thenReturn(characters);
		
		validator.validatePlayerCharacters();
	}


}
