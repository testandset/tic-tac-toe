package com.metro.model;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.metro.config.Constants;
import com.metro.config.GameConfig;
import com.metro.model.player.Player;

@RunWith(SpringRunner.class)
public class BoardTest {

	@MockBean
	GameConfig config;
	
	Board board;

	@Test
	public void shouldCreateBoardEqualToPlaygroundSize() {
		Mockito.when(config.getPlaygroundSize()).thenReturn(3);
		board = new Board(config);
		
		String[][] state = board.getCurrentStatus();
		
		assertTrue(state.length == 3);
		assertTrue(state[0].length == 3);		
	}

	@Test
	public void shouldUpateBoardWhenMovedwithValidValues() {
		Player player = Mockito.mock(Player.class);
		Mockito.when(player.getCharacter()).thenReturn("x");
		
		Position position = Position.buildFromInput("1,1");
		
		Mockito.when(config.getPlaygroundSize()).thenReturn(3);
		board = new Board(config);
		
		board.move(player, position);		
		
		String[][] state = board.getCurrentStatus();
		
		assertTrue(state[1][1] == "x");		
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowExceptionForMoveWithInvalidRow() {
		Player player = Mockito.mock(Player.class);
		Mockito.when(player.getCharacter()).thenReturn("x");
		
		Position position = Position.buildFromInput("4,1");
		
		Mockito.when(config.getPlaygroundSize()).thenReturn(3);
		board = new Board(config);
		
		board.move(player, position);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowExceptionForMoveWithInvalidColumn() {
		Player player = Mockito.mock(Player.class);
		Mockito.when(player.getCharacter()).thenReturn("x");
		
		Position position = Position.buildFromInput("1,4");
		
		Mockito.when(config.getPlaygroundSize()).thenReturn(3);
		board = new Board(config);
		
		board.move(player, position);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowExceptionForMoveWithInvalidRowAndColumn() {
		Player player = Mockito.mock(Player.class);
		Mockito.when(player.getCharacter()).thenReturn("x");
		
		Position position = Position.buildFromInput("4,4");
		
		Mockito.when(config.getPlaygroundSize()).thenReturn(3);
		board = new Board(config);
		
		board.move(player, position);
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowExceptionForMoveWhenBoardIsFull() {
		Player player = Mockito.mock(Player.class);
		Mockito.when(player.getCharacter()).thenReturn("x");
		
		Mockito.when(config.getPlaygroundSize()).thenReturn(2);
		board = new Board(config);
		
		board.move(player, Position.buildFromInput("0,0"));
		board.move(player, Position.buildFromInput("1,1"));
		board.move(player, Position.buildFromInput("0,1"));
		board.move(player, Position.buildFromInput("1,0"));
		
		board.move(player, Position.buildFromInput("1,1"));
	}

	@Test(expected=IllegalArgumentException.class)
	public void shouldThrowExceptionForARedundantMove() {
		Player player = Mockito.mock(Player.class);
		Mockito.when(player.getCharacter()).thenReturn("x");
		
		Mockito.when(config.getPlaygroundSize()).thenReturn(2);
		board = new Board(config);
		
		board.move(player, Position.buildFromInput("0,0"));

		
		board.move(player, Position.buildFromInput("0,0"));
	}
	
	@Test
	public void shoudResetBoard() {
		Player player = Mockito.mock(Player.class);
		Mockito.when(player.getCharacter()).thenReturn("x");
		
		Mockito.when(config.getPlaygroundSize()).thenReturn(2);
		board = new Board(config);
		
		board.move(player, Position.buildFromInput("0,0"));
		board.move(player, Position.buildFromInput("1,1"));
		board.move(player, Position.buildFromInput("0,1"));
		board.move(player, Position.buildFromInput("1,0"));
		
		String[][] state = board.getCurrentStatus();
		
		assertTrue(state[0][0] == "x");
		
		board.resetBoard();
		
		state = board.getCurrentStatus();
		
		assertTrue(state[0][0] == null);
		assertTrue(state[0][1] == null);
		assertTrue(state[1][0] == null);
		assertTrue(state[1][1] == null);
	}
	
	@Test
	public void shouldMarkBoardFullWhenItsFull() {
		Player player = Mockito.mock(Player.class);
		Mockito.when(player.getCharacter()).thenReturn("x");
		
		Mockito.when(config.getPlaygroundSize()).thenReturn(2);
		board = new Board(config);
		
		board.move(player, Position.buildFromInput("0,0"));
		board.move(player, Position.buildFromInput("1,1"));
		board.move(player, Position.buildFromInput("0,1"));
		board.move(player, Position.buildFromInput("1,0"));
		
		assertTrue(board.isBoardFull());		
	}
	
	@Test
	public void shouldCheckForRowWin() {
		Player player = Mockito.mock(Player.class);
		Mockito.when(player.getCharacter()).thenReturn("x");
		
		Mockito.when(config.getPlaygroundSize()).thenReturn(3);
		board = new Board(config) {
			protected void signalWin(String palyer, String win) {
				// do nothing.
			}
		};
		
		Board spyBoard = Mockito.spy(board);
		Mockito.doNothing().when(spyBoard).signalWin(Mockito.anyString(), Mockito.anyString());
		
		spyBoard.move(player, Position.buildFromInput("0,0"));
		spyBoard.move(player, Position.buildFromInput("0,1"));
		spyBoard.move(player, Position.buildFromInput("0,2"));
		
		spyBoard.checkForRowWin();
		
		Mockito.verify(spyBoard, Mockito.times(1)).signalWin("x", Constants.WIN_BY_ROW);
	}
	
	@Test
	public void shouldCheckForColumnWin() {
		Player player = Mockito.mock(Player.class);
		Mockito.when(player.getCharacter()).thenReturn("x");
		
		Mockito.when(config.getPlaygroundSize()).thenReturn(3);
		board = new Board(config) {
			protected void signalWin(String palyer, String win) {
				// do nothing.
			}
		};
		
		Board spyBoard = Mockito.spy(board);
		Mockito.doNothing().when(spyBoard).signalWin(Mockito.anyString(), Mockito.anyString());
		
		spyBoard.move(player, Position.buildFromInput("0,1"));
		spyBoard.move(player, Position.buildFromInput("1,1"));
		spyBoard.move(player, Position.buildFromInput("2,1"));
		
		spyBoard.checkForColumnWin();
		
		Mockito.verify(spyBoard, Mockito.times(1)).signalWin("x", Constants.WIN_BY_COLUMN);
	}
	
	@Test
	public void shouldCheckForMainDiagonalWin() {
		Player player = Mockito.mock(Player.class);
		Mockito.when(player.getCharacter()).thenReturn("x");
		
		Mockito.when(config.getPlaygroundSize()).thenReturn(3);
		board = new Board(config) {
			protected void signalWin(String palyer, String win) {
				// do nothing.
			}
		};
		
		Board spyBoard = Mockito.spy(board);
		Mockito.doNothing().when(spyBoard).signalWin(Mockito.anyString(), Mockito.anyString());
		
		spyBoard.move(player, Position.buildFromInput("0,0"));
		spyBoard.move(player, Position.buildFromInput("1,1"));
		spyBoard.move(player, Position.buildFromInput("2,2"));
		
		spyBoard.checkForDiagonalWin();
		
		Mockito.verify(spyBoard, Mockito.times(1)).signalWin("x", Constants.WIN_BY_DIAGONAL);
	}

	@Test
	public void shouldCheckForAntiDiagonalWin() {
		Player player = Mockito.mock(Player.class);
		Mockito.when(player.getCharacter()).thenReturn("x");
		
		Mockito.when(config.getPlaygroundSize()).thenReturn(3);
		board = new Board(config) {
			protected void signalWin(String palyer, String win) {
				// do nothing.
			}
		};
		
		Board spyBoard = Mockito.spy(board);
		Mockito.doNothing().when(spyBoard).signalWin(Mockito.anyString(), Mockito.anyString());
		
		spyBoard.move(player, Position.buildFromInput("0,2"));
		spyBoard.move(player, Position.buildFromInput("1,1"));
		spyBoard.move(player, Position.buildFromInput("2,0"));
		
		spyBoard.checkForDiagonalWin();
		
		Mockito.verify(spyBoard, Mockito.times(1)).signalWin("x", Constants.WIN_BY_DIAGONAL);
	}
	
	@Test
	public void shouldReturnEmptyPositionOnBaord() {
		Player player = Mockito.mock(Player.class);
		Mockito.when(player.getCharacter()).thenReturn("x");
		
		Mockito.when(config.getPlaygroundSize()).thenReturn(3);
		board = new Board(config);
			
		board.move(player, Position.buildFromInput("0,2"));
		board.move(player, Position.buildFromInput("1,1"));
		board.move(player, Position.buildFromInput("2,0"));
		
		List<String> emptyPositions = board.getEmptyPositions();
		
		assertTrue(emptyPositions.size() == 6);
		assertTrue(emptyPositions.contains("0,0"));
		assertTrue(emptyPositions.contains("0,1"));
		assertTrue(emptyPositions.contains("1,0"));
		assertTrue(emptyPositions.contains("1,2"));
		assertTrue(emptyPositions.contains("2,1"));
		assertTrue(emptyPositions.contains("2,2"));		
	}






}
