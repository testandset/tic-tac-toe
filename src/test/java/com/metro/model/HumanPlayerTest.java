package com.metro.model;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.metro.model.player.HumanPlayer;
import com.metro.ui.io.Reader;
import com.metro.ui.io.Writer;

@RunWith(SpringRunner.class)
public class HumanPlayerTest {
	
	@MockBean
    Writer out;
	
	@MockBean
    Reader in;
	
	@InjectMocks
	HumanPlayer player;
	
	@Before
	public void prepare() {
	    MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldTakeNextMoveFromConsole() {
		Mockito.when(in.read()).thenReturn("1,2");
		
		Position position = player.getNextMove();
		assertTrue(position.getRow() == 1);
		assertTrue(position.getColumn() == 2);
	}

}
