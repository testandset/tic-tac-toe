package com.metro.model;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.metro.model.player.AIPlayer;
import com.metro.model.player.HumanPlayer;
import com.metro.model.player.Player;
import com.metro.model.player.builder.PlayerBuilder;

@RunWith(SpringRunner.class)
public class PlayerBuilderTest {
	
	@MockBean
	AutowireCapableBeanFactory beanFactory;
	
	@InjectMocks
	PlayerBuilder builder;
	
	@Before
	public void prepare() {
	    MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldReturnListOfPlayers() {
		@SuppressWarnings("rawtypes")
		List players = builder.buildPayersFromCharacters(Arrays.asList("a", "b", "c"));
		
		for(Object player: players) {
			assertTrue(player instanceof Player);
		}		
	}
	
	@Test
	public void shouldHaveLastElementOfTypeAIPlayer() {
		List<Player> players = builder.buildPayersFromCharacters(Arrays.asList("a", "b", "c"));
		Player lastPlayer = players.get(players.size() - 1);
		
		assertTrue(lastPlayer instanceof AIPlayer);		
	}
	
	@Test
	public void shouldHaveAtLeaseOneHumanPlayer() {
		List<Player> players = builder.buildPayersFromCharacters(Arrays.asList("a", "b", "c"));
		boolean isHumanPlayerPresent = players
										.stream()
										.anyMatch(p -> p instanceof HumanPlayer);
		
		assertTrue(isHumanPlayerPresent);		
	}

}
