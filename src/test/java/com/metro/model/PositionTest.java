package com.metro.model;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import com.metro.game.exception.InvalidMoveException;

public class PositionTest {

	
	@Before
	public void prepare() {
	    MockitoAnnotations.initMocks(this);
	}

	@Test
	public void shouldBuildSuccessfullyForValidInputs() {
		Position pos = Position.buildFromInput("1,2");
		assertTrue(pos.getRow() == 1);
		assertTrue(pos.getColumn() == 2);
	}

	@Test(expected=InvalidMoveException.class)
	public void shoudThrowExceptionForInvalidInputWithOnlyOneInteger() {
		Position.buildFromInput("12");
	}
	
	@Test(expected=InvalidMoveException.class)
	public void shoudThrowExceptionForInvalidInputWithOneIntegerAndComma() {
		Position.buildFromInput("12,");
	}
	
	@Test(expected=NumberFormatException.class)
	public void shoudThrowExceptionForInvalidInputWithCommaAndInteger() {
		Position.buildFromInput(",12");
	}
	
	@Test(expected=NumberFormatException.class)
	public void shouldThrowExceptionForNonIntegerInputs() {
		Position.buildFromInput("a,12");
	}
	
	@Test(expected=NumberFormatException.class)
	public void shouldThrowExceptionForLargeInteges() {
		Position.buildFromInput("10000000000000000,12");
	}

}
