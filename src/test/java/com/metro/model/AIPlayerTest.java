package com.metro.model;

import static org.junit.Assert.*;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import com.metro.model.player.AIPlayer;
import com.metro.ui.io.Writer;

@RunWith(SpringRunner.class)
public class AIPlayerTest {
	
	@MockBean
	Board board;
	
	@MockBean
	Writer out;
	
	@InjectMocks
	AIPlayer player;
	
	@Before
	public void prepare() {
	    MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void shouldPickARandomPositionFromAvailablePositions() {
		List<String> availablePositions = Arrays.asList("1,2", "2,3", "3,4");
		Mockito.when(board.getEmptyPositions()).thenReturn(availablePositions);
		
		Position position = player.getNextMove();
		assertTrue(availablePositions.contains(position.getRow() + "," + position.getColumn()));
	}

	@Test
	public void shouldPickPositionWhenOnlyOnePositionIsAvailable() {
		Mockito.when(board.getEmptyPositions()).thenReturn(Arrays.asList("1,2"));
		
		Position position = player.getNextMove();
		
		assertTrue(position.getRow() == 1);
		assertTrue(position.getColumn() == 2);
	}

}
