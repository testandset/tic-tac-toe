package com.metro;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.metro.config.GameConfig;
import com.metro.game.GameEngine;
import com.metro.model.player.Player;
import com.metro.model.player.builder.PlayerBuilder;
import com.metro.ui.io.Writer;
import com.metro.validate.ArgumentsValidator;

/**
 * Main application that starts the game. It validates all the given input and
 * adds players to the game. Shows error if any validation fails and exits
 * program with -1 error code.
 *
 * @author Deepak.Singh
 *
 */
@SpringBootApplication
public class TicTacToeApp implements CommandLineRunner {
	
	@Autowired
	GameConfig config;

	@Autowired
	ArgumentsValidator validator;

	@Autowired
	GameEngine gameEngine;

	@Autowired
	ShutdownManager shutdownManager;

	@Autowired
	PlayerBuilder playerBuilder;

	@Autowired
	Writer out;

	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args) {

		SpringApplication application = new SpringApplication(TicTacToeApp.class);
		application.run(args);

	}

	/**
	 * Override for SpringApplication run. Validates input and starts the game.
	 */
	@Override
	public void run(String... args) throws Exception {

		validateInputArguments();

		addPlayersToGame();

		gameEngine.start();
	}

	/**
	 * Creates Players from given player characters and add them to game engine
	 */
	private void addPlayersToGame() {
		List<String> playerCharacters = Arrays.asList(config.getPlayerCharacters());
		List<Player> players = playerBuilder.buildPayersFromCharacters(playerCharacters);
		
		for (Player player : players) {
			gameEngine.addPlayer(player);
		}
	}

	/**
	 * Validates inputs
	 * 		* playgroundSize
	 * 		* player characters
	 */
	private void validateInputArguments() {
		try {
			validator.validatePlaygroundSize();
			validator.validatePlayerCharacters();
		} catch (IllegalArgumentException iae) {
			out.write(iae.getMessage());
			shutdownManager.initiateShutdown(-1);
		}
	}
}
