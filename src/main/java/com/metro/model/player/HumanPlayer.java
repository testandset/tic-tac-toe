package com.metro.model.player;

import org.springframework.beans.factory.annotation.Autowired;

import com.metro.model.Position;
import com.metro.ui.io.Reader;
import com.metro.ui.io.Writer;

/**
 * Implementation of a Human player. Seems input from a user via Reader interface.
 * 
 * @author Deepak.Singh
 *
 */
public class HumanPlayer implements Player {
	
	@Autowired
	Reader in;
	
	@Autowired
	Writer out;

	private String character;

	public HumanPlayer(String character) {
		this.character = character;
	}

	public String getCharacter() {
		return character;
	}

	/**
	 * Get next move from player from console
	 */
	public Position getNextMove() {
		out.write("Player " + this.character + " your next move?");

		String move = in.read();
		return Position.buildFromInput(move);
	}
}
