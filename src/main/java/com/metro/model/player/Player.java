package com.metro.model.player;

import com.metro.model.Position;

/**
 * Interface for a player. 
 * @author Deepak.Singh
 *
 */
public interface Player {
	
	/**
	 * Get the character of the player
	 * @return
	 */
	public String getCharacter();
	
	/**
	 * Seek next move from player
	 * @return
	 */
	public Position getNextMove();

}
