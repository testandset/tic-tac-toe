package com.metro.model.player;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.metro.model.Board;
import com.metro.model.Position;
import com.metro.ui.io.Writer;

/**
 * The genius AI machine player. Implements Player interface.
 * 
 * It picks out at random from available moves left. What out humans.
 * 
 * @author Deepak.Singh
 *
 */
public class AIPlayer implements Player {
	
	@Autowired
	Board board;
	
	@Autowired
	Writer out;
	
	private String character;
	
	public AIPlayer(String character) {
		this.character = character;
	}
	
	/**
	 * Return player character
	 */
	public String getCharacter() {
		return character;
	}
	
	/**
	 * Implementation of interface method to make next move. Picks a random position from available moves.
	 */
	@Override
	public Position getNextMove() {
		List<String> emptyPositions = board.getEmptyPositions();
		String aiMove = pickRandomPositionFrom(emptyPositions);
		
		out.write("Machine " + this.character + " moves: " + aiMove);
		
		return Position.buildFromInput(aiMove);
		
	}

	private String pickRandomPositionFrom(List<String> emptyPositions) {
		int randomIndex = (int)(Math.random() * emptyPositions.size());
		return emptyPositions.get(randomIndex);
	}

}
