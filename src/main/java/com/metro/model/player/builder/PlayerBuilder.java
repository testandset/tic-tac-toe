package com.metro.model.player.builder;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.stereotype.Component;

import com.metro.model.player.AIPlayer;
import com.metro.model.player.HumanPlayer;
import com.metro.model.player.Player;

/**
 * Player builder. Builds a player list for game.
 * @author Deepak.Singh
 *
 */
@Component
public class PlayerBuilder {
	
	@Autowired
	private AutowireCapableBeanFactory beanFactory;

	/**
	 * Builds a list of player for game. For n characters it creates n-1 HumanPlayers and 1 AIPlayer
	 * @param characters
	 * @return
	 */
	public List<Player> buildPayersFromCharacters(List<String> characters){
		
		// create HumanPlayers
		List<Player> players =  characters
									.subList(0, characters.size() - 1)
									.stream()
									.map(c -> createHumanPlayer(c))
									.collect(Collectors.toList());
		
		// create AIPlayer
		players.add(convertToAIPlayer(new HumanPlayer(characters.get(characters.size() - 1))));
		
		return players;
	}
	
	/**
	 * Create AIPlyaer from a give player
	 * @param player
	 * @return
	 */
	public Player convertToAIPlayer(Player player) {
		Player aiPlayer = new AIPlayer(player.getCharacter());
		beanFactory.autowireBean(aiPlayer);
		return aiPlayer;
	}
	
	/**
	 * Create a HumanPlayer
	 * @param character
	 * @return
	 */
	public Player createHumanPlayer(String character) {
		HumanPlayer roger = new HumanPlayer(character);
		beanFactory.autowireBean(roger);
		return roger;
	}
}
