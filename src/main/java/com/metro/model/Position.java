package com.metro.model;

import com.metro.game.exception.InvalidMoveException;

/**
 * Implementation of a board move. Denotes a (row, column) on a Tic-Tac-Toe board.
 * @author Deepak.Singh
 *
 */
public class Position {
	
	/**
	 * Builder from a string input of the form x,y. 
	 * @param move
	 * @return
	 */
	public static Position buildFromInput(String move) {
		String[] pos = move.split(",");
		
		if(pos.length != 2) {
			throw new InvalidMoveException("Invalid move. Enter only two integers separeted by a comma. Eg. 1,2");
		}
		
		int row = Integer.parseInt(pos[0].trim());
		int column = Integer.parseInt(pos[1].trim());
		
		return new Position(row, column);
	}
	
	private int row;
	
	private int column;
	
	public Position(int r, int c) {
		this.row = r;
		this.column = c;
	}
	
	public int getRow() {
		return this.row;
	}
	
	public int getColumn() {
		return this.column;
	}
}
