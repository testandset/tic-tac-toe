package com.metro.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metro.ShutdownManager;
import com.metro.config.Constants;
import com.metro.config.GameConfig;
import com.metro.model.player.Player;
import com.metro.ui.io.Writer;

/**
 * Implementation of Tic-Tac-Toe board. Maintains an internal representation of
 * the board as a 2D array.
 * 
 * 2D array because its lightening fast and size remain constant throughout the
 * game.
 * 
 * @author Deepak.Singh
 *
 */
@Component
public class Board {

	@Autowired
	ShutdownManager shutdownManager;

	@Autowired
	Writer out;

	private int size;
	private String[][] board;

	/**
	 * To maintain if board is full without traversing the whole matrix.
	 */
	private int marked = 0;

	public Board(GameConfig config) {
		this.size = config.getPlaygroundSize();
		this.board = new String[size][size];
	}

	/**
	 * Marks a given position from a player on board. It validates if its a valid
	 * move and its not been marked before.
	 * 
	 * @param player
	 * @param toPosition
	 */
	public void move(Player player, Position toPosition) {
		if (!isValidPosition(toPosition)) {
			throw new IllegalArgumentException("Invalid Position. Try again. Remember Numbers start from zero!");
		}

		if (isBoardFull()) {
			throw new IllegalArgumentException("Board full!.");
		}

		if (board[toPosition.getRow()][toPosition.getColumn()] == null) {
			board[toPosition.getRow()][toPosition.getColumn()] = player.getCharacter();
			marked++;
		} else {
			throw new IllegalArgumentException("Position already marked. Try again");
		}

	}

	/**
	 * Reset board if player wants to restart the game.
	 */
	public void resetBoard() {
		board = new String[size][size];
		marked = 0;
	}

	/**
	 * Get the current status of the board. It clones the board to guard from
	 * mutability. Clone comes with a performance cost but as the max it can be is
	 * 10 x 10 its easily manageable.
	 * 
	 * @return [][]
	 */
	public String[][] getCurrentStatus() {
		return board.clone();
	}

	/**
	 * Check if board is full
	 * 
	 * @return
	 */
	public boolean isBoardFull() {
		return marked == size * size;
	}

	/**
	 * Checks if there is win so far. Check for all 4 types of win by row, by
	 * column, by main-diagonal and by anti-diagonal.
	 */
	public void checkForWin() {
		checkForRowWin();
		checkForColumnWin();
		checkForDiagonalWin();
	}

	/**
	 * Checks if there are any row wins
	 */
	protected void checkForRowWin() {
		for (int i = 0; i < size; i++) {
			// let set do the work of counting
			Set<String> row = new HashSet<String>();
			for (int j = 0; j < size; j++) {
				row.add(board[i][j]);
				
				// short-cut if we already have two characters in this row, there is now winner
				if (row.size() > 1) {
					break;
				}
			}
			
			// if set size is one and its only element in not null, we have a winner.
			if (row.size() == 1 && row.iterator().next() != null) {
				signalWin(board[i][0], Constants.WIN_BY_ROW);
				break;
			}
		}
	}

	/**
	 * Check if there are any column wins
	 */
	protected void checkForColumnWin() {

		for (int i = 0; i < size; i++) {
			// let set do the work of counting
			Set<String> column = new HashSet<String>();
			for (int j = 0; j < size; j++) {
				column.add(board[j][i]);

				// short-cut if we already have two characters in this column, there is now winner
				if (column.size() > 1) {
					break;
				}
			}

			// if set size is one and its only element in not null, we have a winner.
			if (column.size() == 1 && column.iterator().next() != null) {
				signalWin(board[0][i], Constants.WIN_BY_COLUMN);
				break;
			}
		}
	}

	/**
	 * Check for a diagonal win. Check both main-diagonal and anti-diagonal.
	 */
	protected void checkForDiagonalWin() {
		Set<String> diagonal = new HashSet<String>();
		
		// lets check main-diagonal first
		for (int i = 0; i < size; i++) {
			diagonal.add(board[i][i]);
		}

		if (diagonal.size() == 1 && diagonal.iterator().next() != null) {
			signalWin(board[0][0], Constants.WIN_BY_DIAGONAL);
			return;
		}

		// no win so far, let move on
		diagonal.clear();

		// check anti-diagonal
		for (int i = 0, j = size - 1; i < size && j >= 0; i++, j--) {
			diagonal.add(board[i][j]);
		}

		if (diagonal.size() == 1 && diagonal.iterator().next() != null) {
			signalWin(board[size - 1][0], Constants.WIN_BY_DIAGONAL);
			return;
		}
	}
	
	/**
	 * Signal a win. Prints out message and exits application.
	 * 
	 * @param player
	 * @param winsBy
	 */
	protected void signalWin(String player, String winsBy) {
		out.write("Player " + player + " " + winsBy);
		shutdownManager.initiateShutdown(0);
	}


	/**
	 * Checks if a position is valid for the board.
	 * @param position
	 * @return
	 */
	private boolean isValidPosition(Position position) {
		if (isValid(position.getRow()) && isValid(position.getColumn())) {
			return true;
		}

		return false;
	}

	/**
	 * Checks if value is 0 <= value > size
	 * @param value
	 * @return
	 */
	private boolean isValid(int value) {
		if (value >= size || value < 0) {
			return false;
		}

		return true;
	}

	/**
	 * Get all the empty positions on the board what have not been marked yet.
	 * @return
	 */
	public List<String> getEmptyPositions() {
		List<String> emptyPositions = new ArrayList<String>();

		for (int i = 0; i < size; i++) {
			for (int j = 0; j < size; j++) {
				if (board[i][j] == null) {
					emptyPositions.add(i + "," + j);
				}
			}
		}

		return emptyPositions;
	}

}
