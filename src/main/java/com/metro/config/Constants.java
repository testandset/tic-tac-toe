package com.metro.config;

/**
 * Application constants
 * 
 * @author Deepak.Singh
 *
 */
public class Constants {
	
	public static final String WIN_BY_ROW = "wins by row";
	
	public static final String WIN_BY_COLUMN = "wins by column";
	
	public static final String WIN_BY_DIAGONAL = "wins by diagonal";
	
	public static final String RESERVED = "-";

}
