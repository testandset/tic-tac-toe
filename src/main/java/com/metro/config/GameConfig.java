package com.metro.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Central class to load all game configuration so that we don't have @Value
 * annotations littered around code. Makes it easy to manage.
 * 
 * @author Deepak.Singh
 *
 */
@Component
public class GameConfig {

	@Value("${playground.size}")
	int playgroundSize;

	@Value("${player.characters}")
	String playerCharacters;

	@Value("${total.players}")
	int totalPlayers;

	@Value("${playground.max.size}")
	int maxPlaygroundSize;

	@Value("${playground.min.size}")
	int minPlaygroundSize;

	public int getPlaygroundSize() {
		return playgroundSize;
	}

	public void setPlaygroundSize(int size) {
		this.playgroundSize = size;
	}

	public String[] getPlayerCharacters() {
		return playerCharacters.split(",");
	}

	public void setPlayerCharacters(String chars) {
		this.playerCharacters = chars;
	}

	public int getTotalPlayers() {
		return totalPlayers;
	}

	public void setTotalPlayers(int players) {
		this.totalPlayers = players;
	}

	public int getMinimumPalygroundSize() {
		return minPlaygroundSize;
	}

	public void setMinimumPlaygroundSize(int size) {
		this.minPlaygroundSize = size;
	}

	public int getMaxPalygroundSize() {
		return maxPlaygroundSize;
	}

	public void setMaxPlaygroundSize(int size) {
		this.maxPlaygroundSize = size;
	}
}
