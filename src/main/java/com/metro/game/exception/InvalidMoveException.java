package com.metro.game.exception;

/**
 * Application error for invalid moves.
 * @author Deepak.Singh
 *
 */
public class InvalidMoveException extends RuntimeException {
	
	private static final long serialVersionUID = 1L;

	public InvalidMoveException(String error) {
		super(error);
	}

}
