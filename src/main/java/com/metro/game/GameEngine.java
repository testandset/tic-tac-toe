package com.metro.game;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metro.config.GameConfig;
import com.metro.game.exception.InvalidMoveException;
import com.metro.model.Board;
import com.metro.model.player.Player;
import com.metro.model.Position;
import com.metro.ui.BoardPrinter;
import com.metro.ui.io.Writer;

/**
 * Game engine is the manager of the game. Its the dealer in the Poker if you
 * will. Its makes sure everybody has fair turns. Prompts users to make moves
 * and changes the board accordingly. If a user makes an error or makes an
 * invalid move, it prompts user to input again.
 * 
 * Its picks a random player to begin with and then cycles through all players
 * till board is full.
 * 
 * @author Deepak.Singh
 *
 */
@Component
public class GameEngine {

	@Autowired
	GameConfig config;

	@Autowired
	Board board;

	@Autowired
	BoardPrinter boardPrinter;

	@Autowired
	Greeter greeter;

	@Autowired
	Writer out;

	List<Player> players = new ArrayList<Player>();

	/**
	 * Add players to the game.
	 * 
	 * @param player
	 */
	public void addPlayer(Player player) {
		players.add(player);
	}

	/**
	 * Returns all players. Mainly for testing.
	 * 
	 * Clones the player list before returning to guard against mutability.
	 * 
	 * @return
	 */
	protected List<Player> getPlayers() {
		return new ArrayList<>(players);
	}

	/**
	 * The main logic of the game. This is the juice.
	 */
	public void start() {

		int currnetPlayerIndex = getRandomIndex();

		Player player = players.get(currnetPlayerIndex);

		greeter.greet();

		printBoard();

		while (boardIsNotFull()) {
			makeMove(player);

			printBoard();

			checkForWins();

			currnetPlayerIndex = getNextIndex(currnetPlayerIndex);

			player = players.get(currnetPlayerIndex);
		}

		// We are done, check-out! Its a draw
		out.write("Match draw. Well played everyone!");
	}

	private boolean boardIsNotFull() {
		return !board.isBoardFull();
	}

	private void checkForWins() {
		board.checkForWin();
	}

	private void printBoard() {
		String currentStatus = boardPrinter.printBoard(board.getCurrentStatus());
		out.write(currentStatus);
	}

	private int getNextIndex(int playerIndex) {
		return (playerIndex + 1) % players.size();
	}

	protected int getRandomIndex() {
		return (int) (Math.random() * players.size());
	}

	/**
	 * Makes a move on board for a given player. If a move is invalid it prompts
	 * player to give another input.
	 * 
	 * @param player
	 */
	protected void makeMove(Player player) {
		Position position = getNextMoveFrom(player);

		try {

			board.move(player, position);

		} catch (IllegalArgumentException exception) {
			// Got an error, prompt user and seek another input
			out.write(exception.getMessage());
			makeMove(player);
		}
	}

	/**
	 * Get next move from given player. If he messes up prompt him/her again. We
	 * only accept valid moves.
	 * 
	 * @param player
	 * @return Position 
	 */
	protected Position getNextMoveFrom(Player player) {
		Position position;
		try {
			position = player.getNextMove();
		} catch (InvalidMoveException exception) {
			out.write(exception.getMessage());
			return getNextMoveFrom(player);
		} catch (NumberFormatException exception) {
			out.write("Invalid integer, try again! Eg. 1,2 ");
			return getNextMoveFrom(player);
		}

		return position;
	}
}
