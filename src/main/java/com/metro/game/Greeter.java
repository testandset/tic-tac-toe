package com.metro.game;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metro.ui.io.Writer;

/**
 * Show the greeting message and rules of the game to user.
 * 
 * @author Deepak.Singh
 *
 */
@Component
public class Greeter {
	
	@Autowired
	Writer out;
	
	String newLine = System.getProperty("line.separator");
	
	public void greet() {
		StringBuilder message = new StringBuilder();
		
		message.append(newLine);
		message.append("Wecome to Tic-Tac-Toe with a twist!");
		message.append(newLine);
		message.append(newLine);
		message.append(" * You are playing against a genius AI computer.");
		message.append(newLine);
		message.append(" * When it's your turn, enter two integers seperated by a comma to make a move. e.g 1,2.");
		message.append(newLine);
		message.append(" * This would mean that you want to mark the cell corresponding to row 1 and column 2.");
		message.append(newLine);
		message.append(" * Both row and column numbers start from 0.");
		message.append(newLine);
		message.append(" * Numbers go from left to right and from top to bottom. So top left is 0,0. Got it?");
		message.append(newLine);
		message.append(" * Hyphens \"-\" mark empty spaces on board.");
		message.append(newLine);
		message.append(newLine);
		message.append("May the best player win!");
		message.append(newLine);
		message.append("--------------------------------------------------------------------------------------");

		out.write(message.toString());
	}
}
