package com.metro.validate;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.metro.config.Constants;
import com.metro.config.GameConfig;

/**
 * Input argument validator. 
 * Checks
 * 		- if all characters are of single length
 * 		- if there are any many characters as there are players
 * 		- there are no duplicate characters
 * 		- playground size is between [3,10]
 * 		
 * @author Deepak.Singh
 *
 */
@Component
public class ArgumentsValidator {

	@Autowired
	GameConfig config;

	/**
	 * Validate give playground size.
	 */
	public void validatePlaygroundSize() {
		
		int minimumPlaygroundSize = config.getMinimumPalygroundSize();
		int maximumPalygroundSize = config.getMaxPalygroundSize();
		int givenPlaygroundSize = config.getPlaygroundSize();

		boolean isSizeValid = false;

		if (minimumPlaygroundSize == Math.min(minimumPlaygroundSize, givenPlaygroundSize)) {
			isSizeValid = true;
		}

		if (isSizeValid && maximumPalygroundSize != Math.max(maximumPalygroundSize, givenPlaygroundSize)) {
			isSizeValid = false;
		}

		if (!isSizeValid) {
			throw new IllegalArgumentException(
					"Invalid playground size. Allowed [" + minimumPlaygroundSize + " , " + maximumPalygroundSize + "]");
		}

	}

	/**
	 * Validate givne player characters
	 */
	public void validatePlayerCharacters() {
		
		int totalPlayers = config.getTotalPlayers();
		String[] characters = config.getPlayerCharacters();

		checkIsLengthEqual(totalPlayers, characters);
		
		checkForDuplicateCharacters(characters);
		
		checkAllCharactersValid(characters);
		
		checkForReservedCharacters(characters);

	}

	private void checkForReservedCharacters(String[] characters) {
		boolean isInValid = Arrays
							.asList(characters)
							.stream()
							.anyMatch(c -> c.equals(Constants.RESERVED));
		
		if(isInValid) {
			throw new IllegalArgumentException(
					"Please don't use reserved characters " + Constants.RESERVED);			
		}		
	}

	private void checkAllCharactersValid(String[] characters) {
		boolean isValid = Arrays
							.asList(characters)
							.stream()
							.allMatch(c -> c.length() == 1);
		
		if(!isValid) {
			throw new IllegalArgumentException(
					"Invalid player characters. All characters hould be length 1");			
		}
	}

	private void checkIsLengthEqual(int totalPlayers, String[] characters) {
		if(totalPlayers != characters.length) {
			throw new IllegalArgumentException(
					"Specify characters for all players. Need " + totalPlayers + " characters. Found " + characters.length);			
		}
	}
	
	private void checkForDuplicateCharacters(String[] characters) {
		Set<String> mySet = new HashSet<String>(Arrays.asList(characters));
		
		if(mySet.size() != characters.length) {
			throw new IllegalArgumentException(
					"Duplicate player characters found. All characters should be unique");			
			
		}
	}
}
