package com.metro.ui;

/**
 * Interface for a BoardPrinter
 * @author Deepak.Singh
 *
 */
public interface BoardPrinter {
	
	public String printBoard(final String[][] board);

}
