package com.metro.ui;

import org.springframework.stereotype.Component;

/**
 * Console test printer implementation of BoardPrinter
 * 
 * @author Deepak.Singh
 *
 */
@Component
public class ConsoleBoardPrinter implements BoardPrinter {	

	/**
	 * Gives a textual representation of the board.
	 */
	@Override
	public String printBoard(final String[][] board) {
		StringBuilder boardBuilder = new StringBuilder();
		
		boardBuilder.append(System.getProperty("line.separator"));
		
		for(int i = 0; i < board.length; i++) {
			for(int j = 0; j < board[i].length; j++) {
				String value = board[i][j];
				
				if(value == null) {
					value = "-";
				}
				
				boardBuilder.append(value);
				boardBuilder.append(" ");
			}
			
			boardBuilder.append(System.getProperty("line.separator"));
		}
		
		return boardBuilder.toString();
	}

}
