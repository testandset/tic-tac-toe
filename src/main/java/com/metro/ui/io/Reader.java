package com.metro.ui.io;

/**
 * Interface to read from an input source.
 * @author Deepak.Singh
 *
 */
public interface Reader {
	
	public String read();

}
