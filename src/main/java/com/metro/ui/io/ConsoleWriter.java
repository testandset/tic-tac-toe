package com.metro.ui.io;

import org.springframework.stereotype.Component;

/**
 * Console writer implementation of Write. Prints messages out to console.
 * @author Deepak.Singh
 *
 */
@Component
public class ConsoleWriter implements Writer {

	/**
	 * Print to standard out.
	 */
	@Override
	public void write(String message) {
		System.out.println(message);
	}
}
