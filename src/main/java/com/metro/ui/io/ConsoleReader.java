package com.metro.ui.io;

import java.util.Scanner;

import org.springframework.stereotype.Component;

/**
 * Console reader implementation of Reader interface.
 * @author Deepak.Singh
 *
 */
@Component
public class ConsoleReader implements Reader {

	/**
	 * Reads from standard input
	 */
	@Override
	public String read() {
		Scanner scanner = getScanner();
		return scanner.next();
	}
	
	protected Scanner getScanner() {
		return new Scanner(System.in);
	}
}
