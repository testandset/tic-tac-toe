package com.metro.ui.io;

/**
 * Interface to write to an output source
 * @author Deepak.Singh
 *
 */
public interface Writer {

	public void write(String message);
}
