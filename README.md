# Tic-Tac-Toe
------------------------------------

[TOC]

## Overview
------------
Tic-Tac-Toe is game implemented in Java.

We want to bring the pen-and-paper game Tic-tac-toe to the digital age, but with a little twist: the size of the playfield should be configurable between 3x3 and 10x10. And we also want the symbols (usually O and X) to be configurable. Also it should be for 3 players instead of just 2.

General Rules: https://en.wikipedia.org/wiki/Tic-tac-toe

The two users will play against each other and against the computer. Who is starting is random. In and output should be on the console. After each move, the new state of the playfield is displayed and the player can enter the next position of their character one after another. The next position should be provided in a format like 3,2. Invalid inputs are expected to be handled appropriately.

Other features:
- Use the programming language you feel most comfortable with
- The game takes 3 inputs:
- Size of the playground. Valid values are between 3 and 10.
- Play character 1, 2 and 3:
- A single character for the human player 1
- A single character for the human player 2
- A single character for the computer
- These configurations should come from a file

## Deployment and execution
----------------------------

### Build from source

To build the application from source install,

* Java 1.8
* Maven

Once you have dependencies installed clone the repo and run this on root folder
```
$ git clone http://testandset@bitbucket.org/testandset/tic-tac-toe.git
$ cd tic-tac-toe
$ mvn clean package
```

This would generate executable JAR file in `target` folder. To [run](#markdown-header-jar-executable) the app execute the JAR file.


### JAR Executable
Run the JAR directly from target folder and execute. All dependencies are in the JAR archive. You would need Java 1.8 installed on your machine to run it.

```
$ java -jar target\tic-tac-toe-1.0.jar
```

## Configuration
----------------
There are two options to override default configuration, 

1. By creating a `application.properties` file and putting it in the same folder as the executable JAR

    eg. to override playground size put this in your properties file:

        playground.size=5

1. By passing them as options in execute command

    eg. to override verride playground size:

        java -jar target\tic-tac-toe-1.0.jar --playground.size=5

## Parameters
1. *playground.size* (default=3): Override the default playground size. Must be between [3,10] (inclusive).
1. *player.characters* (deafult=x,o,z) : Comma separated for characters for players. Must be single length characters. Number of characters must be equal to number of players.


## Solution Design
-------------------

For once, lets do the UML do the talk (for all the tall claims it makes)!
You might have to open the image in another tab by right clicking it. 

![UML](TicTacToe.png)






